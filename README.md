# OneLineMenu

Simple bash script function that allows for full-fledged browsable, searchable and selectable menus in a single terminal line, dynamically declaring a variable after usage.

## Usage

The onelinemenu function receives 4 parameters in this order:

1. **`selopt` :** Defines the name of the dynamic variable to be set on completion.
2. **`prompt` :** The prefixed prompt for the menu.
3. **`options` :** The block of text defining all available options, one per string. Each string will be a value the `selopt` will be declared with on selection.
4. **`escapable` (optional) :** Flag that determines the ability to exit the menu without selecting any option (by pressing [Esc], returning an empty variable), _true_ by default.

### Piping options to script to terminal example

```
$ echo "$(cat <<OPTIONS
option 1
option 2
random option
something else
A complete line, punctuation and all!
Algo en español: ¡olé!
OPTIONS
)" | ./onelinemenu.sh
```

Will give you a menu with the options defined in the _OPTIONS_ text block. Try it yourself!

### Embedded browser

```
./onelinebrowser.sh
```
- Browse your entire working tree strating from the location the script is called from.
- If a file is selected, it will be opened using `xdg-open`.
- [ESC] goes back, exit by going back from root (/)

### On code example (after copying function)

```
...
onelinemenu "dynvar" "Select your option" "one\ntwo\three" false
echo "$dynvar"
```

This will declare `dynvar` variable and set to either "one", "two" or "three" (non escapable menu will forbid "" value) before echoing it.
